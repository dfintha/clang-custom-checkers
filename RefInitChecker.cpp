//=== RefInitChecker.cpp ----------------------------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// RefInitChecker checks for temporary values bound to reference member
// initializations in constructors.
//
//===----------------------------------------------------------------------===//

#if !defined(__clang__) && defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wclass-memaccess"
#endif
#include "clang/StaticAnalyzer/Checkers/BuiltinCheckerRegistration.h"
#include "clang/Basic/Builtins.h"
#include "clang/StaticAnalyzer/Core/Checker.h"
#include "clang/StaticAnalyzer/Core/CheckerManager.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/CheckerContext.h"
#if !defined(__clang__) && defined(__GNUC__)
#pragma GCC diagnostic pop
#endif

using namespace clang;
using namespace ento;

constexpr const char shortDescription[] =
    "Reference member initialization with a potential temporary value.";

constexpr const char longDescription[] =
    "This constructor parameter is used in one or more reference member "
    "initializers, but its default value is a temporary value, whose lifetime "
    "will end at the end of the constructor. [cplusplus.RefInit]";

class RefInitChecker : public Checker<check::ASTDecl<CXXConstructorDecl>> {
public:
  void checkASTDecl(const CXXConstructorDecl *ctorDecl,
                    AnalysisManager &manager, BugReporter &reporter) const;
};

static const Decl *IsGoodInitializer(const CXXCtorInitializer *init,
                                     ASTContext &context) {
  bool isReference = false;
  if (init->isMemberInitializer()) {
    const auto member = init->getMember();
    isReference = (member != nullptr) && (member->getType()->isReferenceType());
  }
  const auto initExpr = init->getInit();
  if (initExpr == nullptr)
    return nullptr;
  const auto declRef = dyn_cast<const DeclRefExpr>(initExpr);
  if (declRef == nullptr)
    return nullptr;
  const auto paramDecl = dyn_cast<const ParmVarDecl>(declRef->getDecl());
  if (paramDecl == nullptr)
    return nullptr;
  if (paramDecl->hasDefaultArg()) {
    const auto defArg = paramDecl->getDefaultArg();
    if (defArg == nullptr)
      return nullptr;
    const auto defTypeRecord = defArg->getType()->getAsCXXRecordDecl();
    if (defTypeRecord != nullptr &&
        defArg->isTemporaryObject(context, defTypeRecord) && isReference) {
      return paramDecl;
    }
  }
  return nullptr;
}

static void EmitBadInitWarning(const Decl *ctorDecl, const Decl *initDecl,
                               const AnalysisManager &manager,
                               BugReporter &reporter) {
  auto checkerManager = manager.getCheckerManager();
  if (checkerManager == nullptr)
    return;
  auto location = PathDiagnosticLocation::createBegin(
      ctorDecl, reporter.getSourceManager());
  reporter.EmitBasicReport(ctorDecl, checkerManager->getCurrentCheckName(),
                           shortDescription, categories::LogicError,
                           longDescription, location,
                           initDecl->getSourceRange());
}

void RefInitChecker::checkASTDecl(const CXXConstructorDecl *ctorDecl,
                                  AnalysisManager &manager,
                                  BugReporter &reporter) const {
  if (ctorDecl == nullptr)
    return;
  auto &AC = manager.getASTContext();
  for (const auto &I : ctorDecl->inits()) {
    if (I == nullptr)
      continue;
    const auto badInit = IsGoodInitializer(I, AC);
    if (badInit != nullptr)
      EmitBadInitWarning(ctorDecl, badInit, manager, reporter);
  }
}

void ento::registerRefInitChecker(CheckerManager &manager) {
  manager.registerChecker<RefInitChecker>();
}

bool ento::shouldRegisterRefInitChecker(const LangOptions &LO) {
  return true;
}

