
# Custom Checkers for the Clang Static Analyzer

## Installation

* Get the latest LLVM & Clang release's source.
* Add the source files and/or descriptions to the following files.
  Information on how to do it can be found in the `ADDITIONS` file.
  * `llvm/tools/clang/StaticAnalyzer/Checkers/CMakeLists.txt`
  * `llvm/tools/clang/include/clang/StaticAnalyzer/Checkers/Checkers.td`
* Create a build folder: `llvm/build`.
* In it, reate makefiles for the project. (`cmake -G "Unix Makefiles" ..`)
* Build clang. (`make clang`)
* Build scan-build. (`make scan-build`)

## Checkers

### TemplateReturnChecker [cplusplus.TemplateReturn]

Checks for missing return statements in function templates, where the return
value is a template argument, as these only compile if the argument is void.

### RefInitChecker [cplusplus.RefInit]

Checks for initializers of reference members. If a reference member is
initialized via an initializer list, and the list contains a reference, with a
temporary constructed value as default value, the reference will be a dangling
reference.
                  
### MissingOverrideChecker [cplusplus.MissingOverride]

Checks for missing override specifiers, where they are applicable.
