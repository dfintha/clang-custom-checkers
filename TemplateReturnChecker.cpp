//=== TemplateReturnChecker.cpp ---------------------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// TemplateReturnChecker emits a warning if an uninstantiated non-void function
// template does not have a return statement.
//
//===----------------------------------------------------------------------===//

#if !defined(__clang__) && defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wclass-memaccess"
#endif
#include "clang/StaticAnalyzer/Checkers/BuiltinCheckerRegistration.h"
#include "clang/AST/DeclTemplate.h"
#include "clang/Basic/Builtins.h"
#include "clang/StaticAnalyzer/Core/Checker.h"
#include "clang/StaticAnalyzer/Core/CheckerManager.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/CheckerContext.h"
#if !defined(__clang__) && defined(__GNUC__)
#pragma GCC diagnostic pop
#endif

using namespace clang;
using namespace ento;

constexpr const char shortDescription[] =
    "Missing return statement inside uninstantiated function templates.";

constexpr const char longDescription[] =
    "This function template does not have any return statements in its body. "
    "Consider making it void, or marking it with a noreturn attribute. "
    "[cplusplus.TemplateReturn]";

class TemplateReturnChecker :
    public Checker<check::ASTDecl<FunctionTemplateDecl>> {
public:
  void checkASTDecl(const FunctionTemplateDecl *D,
                    AnalysisManager &M, BugReporter &BR) const;
};

static bool TraverseForReturnStmt(const Stmt *FB) {
    if (dyn_cast<ReturnStmt>(FB) != nullptr)
        return true;

    for (const auto child : FB->children()) {
        if (child != nullptr && TraverseForReturnStmt(child))
            return true;
    }

    return false;
}

static bool CheckDecl(const FunctionTemplateDecl *D) {
    const FunctionDecl *FD = D->getTemplatedDecl();
    if (FD == nullptr)
        return true;

    const bool fUndefined = !FD->isDefined();
    const bool fNoReturn = FD->isNoReturn();
    const bool fVoid = FD->getReturnType()->isVoidType();

    if (fUndefined || fNoReturn || fVoid)
        return true;

    FD = FD->getDefinition();
    if (FD == nullptr)
        return true;

    const Stmt *FB = FD->getBody();
    if (FB == nullptr)
        return true;

    return TraverseForReturnStmt(FB);
}

static void EmitWarning(const Decl *D,
                        const AnalysisManager &AM,
                        BugReporter &BR) {
  auto CM = AM.getCheckerManager();
  if (CM == nullptr)
    return;
  auto PDL = PathDiagnosticLocation::createBegin(D, BR.getSourceManager());
  BR.EmitBasicReport(D, CM->getCurrentCheckName(), shortDescription,
                     categories::LogicError, longDescription, PDL,
                     D->getSourceRange());
}

void TemplateReturnChecker::checkASTDecl(const FunctionTemplateDecl *D,
                                  AnalysisManager &AM,
                                  BugReporter &BR) const {
  if (D == nullptr)
    return;
  if (!CheckDecl(D))
    EmitWarning(D, AM, BR);
}

void ento::registerTemplateReturnChecker(CheckerManager &CM) {
  CM.registerChecker<TemplateReturnChecker>();
}

bool ento::shouldRegisterTemplateReturnChecker(const LangOptions &LO) {
  return true;
}

