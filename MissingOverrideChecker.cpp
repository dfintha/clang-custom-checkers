//=== MissingOverrideChecker.cpp --------------------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// MissingOverrideChecker checks for overridden virtual functions, that miss
// the override specifier.
//
//===----------------------------------------------------------------------===//

#if !defined(__clang__) && defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wclass-memaccess"
#endif
#include "clang/StaticAnalyzer/Checkers/BuiltinCheckerRegistration.h"
#include "clang/Basic/Builtins.h"
#include "clang/StaticAnalyzer/Core/Checker.h"
#include "clang/StaticAnalyzer/Core/CheckerManager.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/CheckerContext.h"
#if !defined(__clang__) && defined(__GNUC__)
#pragma GCC diagnostic pop
#endif

using namespace clang;
using namespace ento;

constexpr const char shortDescription[] =
    "Missing 'override' specifier.";

constexpr const char longDescription[] =
    "The 'override' specifier is missing, although the function itself "
    "overrides a virtual function. [cplusplus.MissingOverride]";

class MissingOverrideChecker  : public Checker<check::ASTDecl<CXXMethodDecl>> {
public:
  void checkASTDecl(const CXXMethodDecl *methodDecl,
                    AnalysisManager &manager, BugReporter &reporter) const;
};

static void EmitMissingOverrideWarning(const Decl *methodDecl,
                                       const AnalysisManager &manager,
                                       BugReporter &reporter) {
  auto checkerManager = manager.getCheckerManager();
  if (checkerManager == nullptr)
    return;
  auto location = PathDiagnosticLocation::createBegin(
      methodDecl, reporter.getSourceManager());
  reporter.EmitBasicReport(methodDecl, checkerManager->getCurrentCheckName(),
                           shortDescription, categories::LogicError,
                           longDescription, location,
                           methodDecl->getSourceRange());
}

void MissingOverrideChecker::checkASTDecl(const CXXMethodDecl *methodDecl,
                                          AnalysisManager &manager,
                                          BugReporter &reporter) const {
  if (methodDecl == nullptr)
    return;
  if (!methodDecl->isVirtual() || methodDecl->isPure ())
    return;
  if (methodDecl->getCanonicalDecl() != methodDecl)
    return;
  if (methodDecl->size_overridden_methods() == 0U)
    return;
  if (isa<CXXDestructorDecl>(methodDecl))
    return;
  if (!methodDecl->getAttr<OverrideAttr>())
    EmitMissingOverrideWarning(methodDecl, manager, reporter);
}

void ento::registerMissingOverrideChecker(CheckerManager &manager) {
  manager.registerChecker<MissingOverrideChecker>();
}

bool ento::shouldRegisterMissingOverrideChecker(const LangOptions &LO) {
  return true;
}

